import { Tedis, TedisPool } from 'tedis';

import { getNext, insertItem } from '..';

const { REDIS_URL } = process.env;
let pool: TedisPool;

beforeAll(() => {
  pool = new TedisPool({ host: REDIS_URL });
});

beforeEach(async () => {
  const client = await pool.getTedis();
  await client.del('0');
  pool.putTedis(client);
});

test('insertItem', async () => {
  const client = await pool.getTedis();
  const value0 = 'Test value with priority 0';
  const value1 = 'Test value with priority 1';
  const value2 = 'Test value with priority 2';
  const value3 = 'Another test value with priority 2';
  client.lpush('0', value0);
  client.lpush('1', value1);
  client.lpush('2', value2);
  client.lpush('2', value3);
  pool.putTedis(client);
  const result0 = await getNext(3, pool);
  expect(result0).toEqual(value0);
  const result1 = await getNext(3, pool);
  expect(result1).toEqual(value1);
  const result2 = await getNext(3, pool);
  expect(result2).toEqual(value2);
  const result3 = await getNext(3, pool);
  expect(result3).toEqual(value3);
  client.lpush('2', value2);
  const result4 = await getNext(3, pool);
  expect(result4).toEqual(value2);
  const result5 = await getNext(3, pool);
  expect(result5).toBeNull();
});

afterAll(() => {
  pool.release();
});
