import { Tedis, TedisPool } from 'tedis';

import { insertItem } from '..';

const { REDIS_URL } = process.env;
let pool: TedisPool;

beforeAll(() => {
  pool = new TedisPool({ host: REDIS_URL });
});

beforeEach(async () => {
  const client = await pool.getTedis();
  await client.del('0');
  pool.putTedis(client);
});

test('insertItem', async () => {
  const tedis = await pool.getTedis();
  const value = 'This is a test string';
  const priority = 0;
  await insertItem(value, priority, pool);
  const result = await tedis.rpop('0');
  expect(result).toEqual(value);
  pool.putTedis(tedis);
});

afterAll(() => {
  pool.release();
});
