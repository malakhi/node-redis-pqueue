/* eslint-disable import/prefer-default-export */
import { Tedis, TedisPool } from 'tedis';

export async function insertItem(value: string, priority: number, pool: TedisPool): Promise<void> {
  const client: Tedis = await pool.getTedis();
  const res = await client.lpush(priority.toString(), value);
  pool.putTedis(client);
}
