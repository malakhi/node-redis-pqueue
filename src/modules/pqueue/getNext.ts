/* eslint-disable import/prefer-default-export */
import { Tedis, TedisPool } from 'tedis';

export async function getNext(size: number, pool: TedisPool): Promise<string | null> {
  const client = await pool.getTedis();
  let result: string | null = null;
  for (let i = 0; i < size; i++) {
    const list = i.toString();
    // eslint-disable-next-line no-await-in-loop
    result = await client.rpop(list);
    if (result !== null) break;
  }
  return result;
}
