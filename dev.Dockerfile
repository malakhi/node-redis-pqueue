FROM node:lts-buster
ENV NODE_ENV development
WORKDIR /usr/src/app
COPY . .
RUN npm install
RUN npm run clean
RUN npm run build:ts