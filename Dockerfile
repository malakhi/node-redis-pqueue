FROM node:lts-alpine AS builder
WORKDIR /usr/src/app
COPY . .
RUN npm install
RUN npm run clean
RUN npm run build:ts

FROM node:lts-alpine AS prod
ENV NODE_ENV production
WORKDIR /usr/src/app
COPY --from=builder ./usr/src/app/dist ./dist
COPY ["package.json", "package-lock.json", "./"]
RUN npm install --production --silent

CMD node ./dist/main.js